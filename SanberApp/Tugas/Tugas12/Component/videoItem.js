import React from 'react';
import {View,Text,StyleSheet,Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class videoItem extends React.Component {
    render(){
        let video=this.props.video;
        alert(video.id );
        return(
            <View style={styles.Container}>
                <Image source={{uri:video.snippet.thumbnails.medium.url}} style={{height:200}}/>
                <View style={styles.descContainer}>
                    <Image source={{url: "https://randomuser.me/" }} style={{widht:50,height:50,borderRadius:25}}/>
                    <View style={{videoDetails}}>
                        <Text style={style.videoTitle}>{video.snippet.title}</Text>
                    </View>
                </View>
            </View>  
        )
    }
}

const styles = StyleSheet.create({ 
    Container: {
        padding:15
    },
    descContainer:{
        flexDirection:'row',
        paddingTop:14
    },
    videoTitle:{
        fontSize:16,
        color:"#3c3c3c"
    },
    videoDetails:{
        paddingHorizontal:15,
        flex:1
    }

})