import React from 'react';
import {View,Text,StyleSheet,Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import videoItem from "./Component/videoItem";
import data from "./data.json" ;

export default class App extends React.Component {
    render() {
        
        return(
            <View style={styles.Container}>
                <View style={styles.navBar}>
                    <Image source={require('./images/logo.png')} style={{width:98,height:22}}  />
                    <View style={styles.rightNav}>
                        <TouchableOpacity>
                        <Icon style={styles.navItem} name= "search" size={28}/> 
                        </TouchableOpacity>
                        <TouchableOpacity>
                        <Icon style={styles.navItem}name= "account-circle" size={28}/> 
                        </TouchableOpacity>
                </View>
                </View> 
                <View style={styles.body}>
                    <videoItem video={data.items[0]}/>
               </View> 
                <View style={styles.tabBar}> 
                    <TouchableOpacity style={styles.tabItem}> 
                        <Icon name="whatshot" size={25} />
                        <Text style={styles.tabTitle}>Trending</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}> 
                        <Icon name="subscriptions" size={25} />
                        <Text style={styles.tabTitle}>Subscriptions</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}> 
                        <Icon name="home" size={25} />
                        <Text style={styles.tabTitle}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}> 
                        <Icon name="folder" size={25} />
                        <Text style={styles.tabTitle}>library</Text>
                    </TouchableOpacity>
                    
                </View>      
            </View>
        )
    }
}

const styles = StyleSheet.create({ 
    Container: {
        flex: 1,
    },
    navBar:{
        height:55,
        backgroundColor:'white',
        elevation:3,
        flexDirection:"row",
        alignItems:'center',
        justifyContent: 'space-between',
        marginTop:40
    },
    rightNav:{
        flexDirection:  "row"
    },
    navItem:{
        marginLeft:15,
        marginRight:15,
        marginTop:15
    },
    body:{
        flex:1
    },
    tabBar:{
        backgroundColor:"white",
        height:60,
        borderTopWidth:1,
        borderColor:"#E5E5E5",
        flexDirection:'row',
        justifyContent:'space-around'
    },
    tabItem:{
        alignItems:"center",
        justifyContent:"center"
    },
    tabTitle:{
        fontSize:11,
        color: "#3c3c3c",
        paddingTop:4
    }
})